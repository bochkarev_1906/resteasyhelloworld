import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/")
public class JSONService {

    @GET
    @Path("/{param}")
    public Response printMessage(@PathParam("param") String msg) {
        String result = "Restful example : " + msg;
        return Response.status(200).entity(result).build();

    }

    @POST
    @Path("/post")
    @Consumes("application/json")
    public Response createProductInJSON(Product product) {

        String result = "Product created : " + product;
        return Response.status(201).entity(result).build();

    }

}
